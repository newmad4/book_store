# book_store

export store db:
$ python manage.py dumpdata store > store.json

import store db:
$ python manage.py loaddata store.json

create superuser to work with django admin
$ python manage.py createsuperuser

show book list in command line by manage command:
$ python manage.py show_book_list --order desc / asc

change copyright start and end years you could in settings.py in section "Copyright"