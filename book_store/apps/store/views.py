from django.views.generic import ListView, CreateView, UpdateView

from book_store.apps.store.models import Book, RequestLog


class BookListView(ListView):
    model = Book
    template_name = "store/book/list.html"
    content_type = "text/html"
    queryset = Book.objects.all().order_by('title')


class BookCreateView(CreateView):
    model = Book
    template_name = "store/book/edit.html"
    content_type = "text/html"
    fields = '__all__'

    def get_form(self, form_class=None):
        form = super(BookCreateView, self).get_form(form_class)
        form.fields['date_field'].widget.attrs.update({'class': 'datepicker'})
        return form


class BookUpdateView(UpdateView):
    model = Book
    template_name = "store/book/edit.html"
    content_type = "text/html"
    fields = ['title', 'author', 'isbn', 'price', 'publish_date']


class RequestLogListView(ListView):
    model = RequestLog
    queryset = RequestLog.objects.all().order_by('time')
    template_name = "store/request/list.html"
    content_type = "text/html"
    context_object_name = 'requests'
    paginate_by = 10
