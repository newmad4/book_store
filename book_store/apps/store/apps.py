from django.apps import AppConfig


class StoreConfig(AppConfig):
    name = 'book_store.apps.store'

    def ready(self):
        import book_store.apps.store.signals  # noqa
