import datetime
from django.utils import timezone

from django.core.signals import request_started
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.utils.timezone import now

from book_store.apps.store.models import Book, RequestLog
import logging

logger = logging.getLogger("Store_logger")


@receiver(post_save, sender=Book)
def create_book(sender, instance, created, **kwargs):
    """
        signal for logging created or updated books
    """
    logger.info("{datetime} | {id} | {model} | {title} | {action}".format(
        datetime=now(),
        id=instance.id,
        model=instance.__class__.__name__,
        title=instance.title,
        action="created" if created else "updated"
    ))


@receiver(post_delete, sender=Book)
def delete_book(sender, instance, **kwargs):
    """
        signal for logging deleted books
    """
    logger.info("Book with title {} was deleted.".format(instance.title))


@receiver(request_started)
def save_request(sender, environ, **kwargs):
    """
        signal for save http requests
    """
    query = environ['QUERY_STRING']
    query = '?' + query if query else ''
    request_log = RequestLog(
        method=environ['REQUEST_METHOD'],
        host=environ['HTTP_HOST'],
        path=environ['PATH_INFO'],
        query=query,
        time=datetime.datetime.now(tz=timezone.utc)
    )
    request_log.save()

