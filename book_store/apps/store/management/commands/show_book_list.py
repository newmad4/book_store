from django.core.management.base import BaseCommand, CommandError
from book_store.apps.store.models import Book


class Command(BaseCommand):
    help = 'Show books list in command line ordered by publish date.'

    def add_arguments(self, parser):
        parser.add_argument('--order', type=str, help=' asc/desc (order by publish date)',)

    def handle(self, *args, **options):
        order = options.get('order')
        parameters = ['asc', 'desc']
        if order is None or order not in parameters:
            raise CommandError('Please enter "order" parameter: asc or desc.')

        order = '-' if 'desc' in order.lower() else ''
        books = Book.objects.all().order_by('{}publish_date'.format(order))
        for book in books:
            self.stdout.write(self.style.SUCCESS("{id} | {title} | {authors} | {isbn} | {price} | {publish_date}".format(
                id=book.id,
                title=book.title,
                authors=", ".join([str(author) for author in book.author.all()]),
                isbn=book.isbn,
                price=book.price,
                publish_date=book.publish_date,
            )))
