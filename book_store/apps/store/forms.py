from django import forms
from django.contrib.admin.widgets import AdminDateWidget

from book_store.apps.store.models import Book


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(BookForm, self).__init__(*args, **kwargs)
        self.fields['publish_date'] = forms.DateField(input_formats=["%Y-%m-%d"], required=True,
                                                      widget=AdminDateWidget())
