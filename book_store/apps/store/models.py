from django.db import models
from django.urls import reverse
from django.utils import timezone
from isbn_field import ISBNField


class Author(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)


class Book(models.Model):
    title = models.CharField(max_length=100)
    author = models.ManyToManyField(Author)
    isbn = ISBNField(clean_isbn=False)
    price = models.DecimalField(decimal_places=2, max_digits=8)
    publish_date = models.DateField(default=timezone.now)

    def get_absolute_url(self):
        return reverse("book_list")


class RequestLog(models.Model):
    method = models.CharField(max_length=10)
    host = models.CharField(max_length=20)
    path = models.CharField(max_length=100)
    query = models.CharField(max_length=300)
    time = models.DateTimeField()
