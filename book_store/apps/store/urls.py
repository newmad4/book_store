from django.conf.urls import url

from book_store.apps.store.views import BookListView, BookCreateView, BookUpdateView, RequestLogListView

urlpatterns = [
    url(r'^books', BookListView.as_view(), name='book_list'),
    url(r'^book/create/', BookCreateView.as_view(), name='book_create'),
    url(r'^book/(?P<pk>[0-9]+)/update/', BookUpdateView.as_view(), name='book_update'),
    url(r'^requests', RequestLogListView.as_view(), name='requests'),
]
